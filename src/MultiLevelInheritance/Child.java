package MultiLevelInheritance;

public class Child extends Parent{
	
	public void Property3()  {
		System.out.println("Property3 is belongs to child");
	}
	public static void main(String[] args) {
		Child ch = new Child();
		Parent p = new Parent();
		GrantParent gp = new GrantParent();
		ch.Property1();
		ch.Property1();
		ch.Property3();
		p.Property1();
		p.Property1();
		gp.Property1();
	}}