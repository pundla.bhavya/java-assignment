package SingleInheritance;

public class NoteBook3 extends Book3 {
   
    public void write() {
        System.out.println("Writing in the notebook...");
    }

    @Override
    public void read() {
        System.out.println("Reading from the notebook...");
    }

    public void draw() {
        System.out.println("Drawing in the notebook...");
    }
public static void main(String[] args) {
	NoteBook3 n=new NoteBook3();
	n.write();
	n.read();
	n.draw();
}
}