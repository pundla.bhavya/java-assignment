package Arrays;

import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		ArrayList<String> list=new ArrayList<>();
		System.out.println(list.size());
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		System.out.println(list.size()); //element store in consecutive memory location
		
		
		list.add("f"); //adding
		System.out.println(list);
		System.out.println(list.size());
		
		list.set(1, "B1");  //replace
		System.out.println(list);
		
		list.remove(2); //Remove
		System.out.println(list);
		
		list.add("f"); //adding
		System.out.println(list);

	}

}
