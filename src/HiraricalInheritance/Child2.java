package HiraricalInheritance;

public class Child2 extends Parent {
	public void Property3()  {
		System.out.println("Property3 is belongs to child3");
	}
		
	public static void main(String[] args) {
			Child1 ch1 = new Child1();
			Child2 ch2 = new Child2();
			Parent prt = new Parent();
			
			ch1.Property1();
			ch1.Property2();
			ch2.Property1();
			ch2.Property3();
			prt.Property1();
		
}
}