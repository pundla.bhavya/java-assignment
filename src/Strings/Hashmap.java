package Strings;

import java.util.HashMap;

public class Hashmap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        HashMap<String, Integer> map = new HashMap<>();
 
        map.put("bhavya", 10);
        map.put("divya", 30);
        map.put("ramya", 20);
 
        System.out.println("Size of map is "+ map.size());
                                                                     
        System.out.println(map);
 
        if (map.containsKey("bhavya")) {
 
            // Mapping
            Integer a = map.get("bhavya");
 
            System.out.println("value for key"+ " \"bhavya\" is " + a);
        }

	}

}
