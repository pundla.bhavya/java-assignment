package Strings;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter string");
		String str1=sc.nextLine();
		
		StringBuffer sb1=new StringBuffer(str1);
		sb1.reverse();
		String str2=sb1.toString();
		if(str1.equals(str2)) {
			System.out.println("Given String is palindrome");
			
		}else {
			System.out.println("Given String is not palindrome");
		}
		
	}

}
