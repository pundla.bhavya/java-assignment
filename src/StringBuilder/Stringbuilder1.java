package StringBuilder;

public class Stringbuilder1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
        StringBuilder str = new StringBuilder("Java");
        System.out.println("String = "+ str.toString());
                        
 
        // reverse the string
        StringBuilder reverseStr = str.reverse();
 
        System.out.println("Reverse String = "+ reverseStr.toString());
                         
 
        // Append ', '(1) to the String
        str.appendCodePoint(1);
 
        System.out.println("Modified StringBuilder = "+ str);
                         
 
        // get capacity
        int capacity = str.capacity();
 
        System.out.println("StringBuilder = " + str);
        System.out.println("Capacity of StringBuilder = "  + capacity);
                           
    }

	}


